/*
 * Main JS file for Ventspils Augstskola RSS widget
 *
 * Created: 22-01-2013
 * Author: r00m (twitter.com/r00m)
 */

var gDoneButton;
var gInfoButton;

var gFeedURL;
var gLastFetched;
var gParsedFeed;
var gCachedXML;

/*
 * Adding function prototype to Number function stack
 */
Number.prototype.padZero = function(len)
{
	var s = String(this), c = '0';
	len = len || 2;

	while(s.length < len)
		s = c + s;

	return s;
}


/*
 * Gets fired when dashboard is shown. Loads timestamp when
 * last data fetch was performed and also cached XML response
 */
function onDashboardShow()
{
	var lastFetched = widget.preferenceForKey("lastFetched");

	if (lastFetched)
		gLastFetched = lastFetched;


	var cachedXML = widget.preferenceForKey("cachedXML");

	if (cachedXML)
		gCachedXML = cachedXML;

	
	loadXML(gFeedURL);
}


/*
 * Gets fired when dashboard is closing. Saves cached XML
 * response and time when last fetch was performed
 */
function onDashboardHide()
{
	widget.setPreferenceForKey(gCachedXML, "cachedXML");
	widget.setPreferenceForKey(gLastFetched, "lastFetched");
}


/*
 * Loads XML from given URL (also forbids to fetch
 * with intervals less than 15 minutes)
 */
function loadXML(url)
{
	var now = new Date().getTime();

	if (now - gLastFetched > 60*15*1000) // 15m in miliseconds
	{
		var xmlhttp = new XMLHttpRequest();

		xmlhttp.open("GET", url, true);
		
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4)
			{
				processAtomFeed(xmlhttp.responseText);

				gCachedXML = xmlhttp.responseText;
			}
		   	else
		   	{
		   		$("#footer p").html("&mdash; Notiek datu atjaunošana &mdash;");
		   	}
		}

		xmlhttp.send(null);

		gLastFetched = now;
	}
	else
	{
		processAtomFeed(gCachedXML);
	}
}


/*
 * Converts timestamp to relative time
 */
function relativeTime(time)
{
	var date = new Date(time);
	var now = new Date();
	var delta = parseInt((now.getTime() - date.getTime()) / 1000);
	var relativeTime;

	if (delta < 60)
		relativeTime = delta + "s atpakaļ";
	else if (delta < 3600)
		relativeTime = delta/60 + "m atpakaļ";
	else if (delta < 86400 && now.getDate() == date.getDate())
		relativeTime = date.getHours().padZero() + ":" + date.getMinutes().padZero();
	else
		relativeTime = date.getDate().padZero() + "." + (date.getMonth()+1).padZero() + ", " + date.getHours().padZero() + ":" + date.getMinutes().padZero();

	return relativeTime;
}


/*
 * Extracts category name from string
 */
function getCategory(str)
{
	var category = str.substr(0, str.indexOf(":"));

	switch(category)
	{
		case "AutStop":
			return "Autostops";

		case "StudInf":
			return "Studiju informācija";

		case "Student":
			return "Studenti";

		case "Sludinj":
			return "Sludinājumi";

		case "IntPul":
			return "Interešu pulciņi";

		case "Sports":
			return "Sports";

		case "KursuZin":
			return "Studentu Padome";

		case "Datori":
			return "Datori";

		case "VeA N,I":
			return "VeA notikumi, informācija";

		default:
			return str;
	}
}


/*
 * Shows backside of the widget
 */
function showPrefs()
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget)
        widget.prepareForTransition("ToBack");

    front.style.display = "none";
    back.style.display = "block";
 
    if (window.widget)
        setTimeout ('widget.performTransition();', 0);
}


/*
 * Closes widget backside
 */
function hidePrefs()
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget)
        widget.prepareForTransition("ToFront");

    back.style.display="none";
    front.style.display="block";

    if (window.widget)
        setTimeout ('widget.performTransition();', 0);
}


/*
 * Creates canvas and sets resulting image as
 * background image
 */
function drawBackground(page_width, page_height)
{
	var canvas = document.createElement("canvas");
	var context = canvas.getContext("2d");
	var shadow_width = 10;
	var shadow_height = page_height + 3;

	// set dimensions
	canvas.width = 510;
	canvas.height = page_height + 20;

	// set up the drop shadow
	context.shadowOffsetX = 0;
	context.shadowOffsetY = 0;
	context.shadowBlur = shadow_width;
	context.shadowColor = "rgba(0,0,0,0.6)";

	// set up the background to match the content background
	context.fillStyle = "rgb(248,248,248)";
	 
	// draw the box to set the drop shadow
	context.fillRect(shadow_width, 5, page_width, shadow_height);

	// set as background image
	$("body").css("background-image", "url(" + canvas.toDataURL() + ")");
}


/*
 * Processes fetched XML into an array of objects
 * grouped by topic category
 */
function processAtomFeed(xml)
{
	gParsedFeed = new Object();

	var now = new Date(gLastFetched);
	var category;


	$(xml).find("entry").each(function(){
		var feedItem = new Object();

		feedItem.time = relativeTime($(this).find("updated").text());
		feedItem.author = $(this).find("name").text();
		feedItem.link = $(this).find("link").attr("href");
		feedItem.topic = $(this).find("summary").text();
		feedItem.category = category = getCategory($(this).find("title").text());

		if(gParsedFeed[category] === undefined)
			gParsedFeed[category] = new Array();
		
		gParsedFeed[category][gParsedFeed[category].length] = feedItem;
	});

	displayTopics();

	$("#footer p").html(
		"&mdash; Atjaunināts: "
		+ now.getDate().padZero() + "."
		+ (now.getMonth()+1).padZero()
		+ ". plkst. " + now.getHours().padZero()
		+ ":" + now.getMinutes().padZero()
		+ " &mdash;"
	);
}


/*
 * Appends data from gParsedFeed to widget's HTML
 */
function displayTopics()
{
	$("#data").empty();

	$.each(gParsedFeed, function(key, val) {
		$("#data").append("<h2>"+ key +" ("+ val.length +")</h2>");

		for (var i = val.length - 1; i >= 0; i--) {
			$("#data").append(
				'<div class="topic">\
					<a href="'+ val[i].link + '" onclick="widget.openURL(this.href); return false;">'+ val[i].topic +'</a>\
					<div class="time">'+ val[i].time +'</div>\
					<div class="author">'+ val[i].author +'</div>\
					<div style="clear:both;"></div>\
				</div>\
			');
		};
	})
}


/*
 * Fired when DOM is loaded
 */
$(document).ready(function(){
	// set vars based on environment
	if (window.widget)
	{
		gFeedURL = "http://vea.venta.lv/Forum/atom/";

		widget.onshow = onDashboardShow;
		widget.onhide = onDashboardHide;
	}
	else
	{
		gFeedURL = "dummy-data.xml";
	}

	// create canvas element, set it's data as background
	drawBackground(360, 360);

	// setup buttons
	gDoneButton = new AppleGlassButton(document.getElementById("doneButton"), "Done", hidePrefs);
	gInfoButton = new AppleInfoButton(document.getElementById("infoButton"), document.getElementById("front"), "black", "white", showPrefs);

	// fetch data
	loadXML(gFeedURL);
});
